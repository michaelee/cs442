//
//  Department+CoreDataProperties.swift
//  CSVLoader
//
//  Created by Michael Lee on 6/20/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Department {

    @NSManaged var name: String?
    @NSManaged var employees: NSSet?

}
