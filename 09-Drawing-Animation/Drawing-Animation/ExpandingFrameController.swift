import UIKit

class ExpandingFrameController: UIViewController {
  var rotatingView: RotatingView!
  var frameView: FrameView!
  var lastTransform: CGAffineTransform!
  
  override func loadView() {
    view = UIView(frame: UIScreen.mainScreen().applicationFrame)
    view.backgroundColor = UIColor.whiteColor()
    view.multipleTouchEnabled = true
    
    rotatingView = RotatingView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
    rotatingView.center = CGPoint(x: CGRectGetMidX(view.frame), y: CGRectGetMidY(view.frame))
    rotatingView.userInteractionEnabled = false
    view.addSubview(rotatingView)
    
    frameView = FrameView(frame: rotatingView.frame)
    frameView.userInteractionEnabled = false
    frameView.opaque = false
    view.addSubview(frameView)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    lastTransform = CGAffineTransformIdentity
    let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(ExpandingFrameController.rotateView(_:)))
    view.addGestureRecognizer(rotateGesture)
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ExpandingFrameController.changeAnchorPoint(_:)))
    view.addGestureRecognizer(tapGesture)
  }
  
  func rotateView(gesture: UIRotationGestureRecognizer) {
    let transform = CGAffineTransformRotate(lastTransform, gesture.rotation)
    if gesture.state == .Ended {
      lastTransform = transform
    }
    rotatingView.transform = transform
    frameView.frame = rotatingView.frame
  }
  
  func changeAnchorPoint(gesture: UITapGestureRecognizer) {
    let loc = gesture.locationInView(rotatingView)
    rotatingView.anchorPointCoord = loc
    rotatingView.setNeedsDisplay()
    frameView.frame = rotatingView.frame
  }

}
