import UIKit

class HelloWorldView: UIView {

  var touchDownLoc: CGPoint!
  
  var subview: UIView?

  required init?(coder aDecoder: NSCoder) {
    fatalError("View not to be loaded from storyboard")
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    bounds.origin = CGPoint(x: 0, y: 0) // play with this value!
    opaque = false
    backgroundColor = UIColor.darkGrayColor()
    
    subview = UIView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
    subview!.backgroundColor = UIColor.brownColor()
    addSubview(subview!)
  }
  
  override func drawRect(rect: CGRect) {
    UIColor.redColor().set()
    UIRectFrame(bounds)
    
    UIColor.blueColor().set()
    UIRectFrame(frame)

    UIColor.greenColor().set()
    UIRectFrame(CGRect(x: 0, y: 0, width: 100, height: 100))
    
    let string: NSString = "Hello world"
    string.drawAtPoint(CGPoint(x: 1, y: 1),
      withAttributes:
      [NSFontAttributeName: UIFont(name: "Helvetica", size: 18)!,
        NSForegroundColorAttributeName: UIColor.whiteColor()])
    
    string.drawAtPoint(CGPoint(x: 1, y: 100),
      withAttributes: [NSFontAttributeName: UIFont(name: "Helvetica", size: 18)!,
        NSForegroundColorAttributeName: UIColor.yellowColor()])

  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let touch = touches.first
    touchDownLoc = touch?.locationInView(self) // try location in self.superview
    print("Touch at location \(touchDownLoc)")    
  }
  
  override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let touch = touches.first
    let newLoc = touch?.locationInView(self)
    let dx = touchDownLoc.x - newLoc!.x
    let dy = touchDownLoc.y - newLoc!.y
    
    bounds = CGRect(x: bounds.origin.x + dx,
      y: bounds.origin.y + dy,
      width: bounds.size.width,
      height: bounds.size.height)
    print("New bounds = \(bounds)")

     setNeedsDisplay() // try removing this
  }
}
