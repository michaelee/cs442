import UIKit

enum Demos {
  case FrameBounds, Scrolling, Rotating, Bezier, Anim
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIScrollViewDelegate {

  var window: UIWindow?

  var imageView = UIImageView(image: UIImage(named: "chicago.jpg"))

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    var controller = UIViewController()
    
    let demo: Demos = .Anim
    
    switch (demo) {
    case .FrameBounds:
      controller.view = HelloWorldView(frame: UIScreen.mainScreen().applicationFrame)
      
    case .Scrolling:
      let scrollView = UIScrollView(frame: UIScreen.mainScreen().applicationFrame)
      scrollView.addSubview(imageView)
      scrollView.contentSize = CGSize(width: 1600, height: 1200)
      scrollView.backgroundColor = UIColor.blackColor()
      scrollView.minimumZoomScale = 0.2
      scrollView.maximumZoomScale = 5.0
      scrollView.delegate = self
      controller.view = scrollView
      
    case .Rotating:
      controller = ExpandingFrameController()
      
    case .Bezier:
      controller.view = BezierDrawing(frame: UIScreen.mainScreen().applicationFrame)
      
    case .Anim:
      let view = controller.view
      view.backgroundColor = UIColor.whiteColor()
      let rotatingView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
      rotatingView.center = CGPoint(x: CGRectGetMidX(view.bounds), y: CGRectGetMidY(view.bounds))
      rotatingView.backgroundColor = UIColor.redColor()
      view.addSubview(rotatingView)
      UIView.animateWithDuration(3.0, delay: 0, options: [.Repeat, .Autoreverse],
                                 animations: {
                                  rotatingView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        }, completion: nil)
    }
    
    window!.rootViewController = controller
    window!.makeKeyAndVisible()
    return true
  }
  
  func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
    return imageView
  }
}

