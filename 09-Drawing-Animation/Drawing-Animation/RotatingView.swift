import UIKit

class RotatingView: UIView {
  var anchorPointCoord: CGPoint! {
    didSet {
      layer.anchorPoint = CGPoint(x: anchorPointCoord.x / CGRectGetMaxX(bounds),
        y: anchorPointCoord.y / CGRectGetMaxY(bounds))
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.grayColor()
    anchorPointCoord = CGPoint(x: bounds.width / 2.0, y: bounds.height / 2.0)
  }
  
  override func drawRect(rect: CGRect) {
    let context: CGContext! = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    // draw rect at anchorpoint
    UIColor.redColor().set()
    UIRectFill(CGRect(x: anchorPointCoord.x-5, y: anchorPointCoord.y-5, width: 10, height: 10));
    
    // draw view contents
    for i in 0..<10 {
      let toDraw: NSString = "\(i)"
      toDraw.drawAtPoint(CGPoint(x: CGRectGetMidX(bounds), y: 0),
        withAttributes: [NSFontAttributeName: UIFont(name: "Helvetica", size: 18)!,
          NSForegroundColorAttributeName: UIColor.blackColor()])
      CGContextRotateCTM(context, CGFloat(M_PI_2/10.0));
    }

    CGContextRestoreGState(context);
  }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
