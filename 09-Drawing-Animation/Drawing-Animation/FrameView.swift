import UIKit

class FrameView: UIView {
  override func drawRect(rect: CGRect) {
    UIColor.redColor().set()
    UIRectFrame(bounds)
  }
}
