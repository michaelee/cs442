import UIKit

class BezierDrawing: UIView {
  var path: UIBezierPath = UIBezierPath()
  
  var isFirstPoint: Bool = true
  var firstPoint: CGPoint?
  
  var numCurrPoints = 0
  var control1, control2, endPoint: CGPoint?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.whiteColor()
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }

  override func drawRect(rect: CGRect) {
    let context: CGContext! = UIGraphicsGetCurrentContext()
    let dashlengths: [CGFloat] = [2.0, 2.0] // used for drawing dashed lines

    CGContextSaveGState(context);
    
    UIColor.blackColor().setStroke()
    UIColor.redColor().setFill()

    if path.empty {
      return
    }
    
    self.path.stroke()
    
    let currPath = UIBezierPath()
    currPath.moveToPoint(path.currentPoint)
    switch numCurrPoints {
    case 1:
      currPath.addLineToPoint(endPoint!)
      currPath.stroke()
      drawDot(endPoint!)
      
    case 2:
      currPath.addQuadCurveToPoint(endPoint!, controlPoint: control1!)
      currPath.stroke()
      currPath.removeAllPoints()
      
      currPath.setLineDash(dashlengths, count: 2, phase: 0)
      currPath.moveToPoint(endPoint!)
      currPath.addLineToPoint(control1!)
      currPath.addLineToPoint(path.currentPoint)
      currPath.stroke()
      drawDot(endPoint!)
      drawDot(control1!)
      
    case 3:
      currPath.addCurveToPoint(endPoint!, controlPoint1: control1!, controlPoint2: control2!)
      currPath.stroke()
      currPath.removeAllPoints()
      
      currPath.setLineDash(dashlengths, count: 2, phase: 0)
      currPath.moveToPoint(endPoint!)
      currPath.addLineToPoint(control2!)
      currPath.moveToPoint(control1!)
      currPath.addLineToPoint(path.currentPoint)
      currPath.stroke()
      drawDot(endPoint!)
      drawDot(control1!)
      drawDot(control2!)
      
    default:
      break
    }
    drawDot(path.currentPoint)
    CGContextRestoreGState(context);
  }
  
  func drawDot(center: CGPoint) {
    let context = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(context, CGRect(x: center.x-5, y: center.y-5, width: 10, height: 10));
    CGContextFillPath(context);
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let loc = touches.first?.locationInView(self)
    if isFirstPoint {
      return;
    }
    switch numCurrPoints {
    case 0:
      endPoint = loc;
    case 1:
      control1 = loc;
    case 2:
      control2 = loc;
    default:
      return
    }
    numCurrPoints += 1;
    setNeedsDisplay()
  }
  
  override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let loc = touches.first?.locationInView(self)
    if isFirstPoint {
      return;
    }
    switch numCurrPoints {
    case 1:
      endPoint = loc
    case 2:
      control1 = loc
    case 3:
      control2 = loc
    default:
      return
    }
    setNeedsDisplay()
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let loc = touches.first?.locationInView(self)
    if isFirstPoint {
      isFirstPoint = false
      path.moveToPoint(loc!)
    } else if numCurrPoints == 3 {
      path.addCurveToPoint(endPoint!, controlPoint1: control1!, controlPoint2: control2!)
      numCurrPoints = 0
    }
    setNeedsDisplay()
  }
  
  override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    if numCurrPoints > 0 {
      numCurrPoints -= 1
    }
    setNeedsDisplay()
  }
}
