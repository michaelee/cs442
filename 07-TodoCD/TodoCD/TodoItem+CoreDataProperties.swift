//
//  TodoItem+CoreDataProperties.swift
//  TodoCD
//
//  Created by Michael Lee on 6/15/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension TodoItem {

    @NSManaged var text: String?
    @NSManaged var created: NSDate?
    @NSManaged var context: Context?

}
