//
//  DetailViewController.swift
//  TodoCD
//
//  Created by Michael Lee on 6/15/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController, UITextFieldDelegate {

  @IBOutlet weak var textField: UITextField!

  @IBOutlet weak var contextField: UITextField!

  var detailItem: TodoItem? {
    didSet {
        // Update the view.
        self.configureView()
    }
  }

  func configureView() {
    // Update the user interface for the detail item.
    if let detail = self.detailItem {
      if let field = self.textField {
        field.text = detail.text
      }
      if let context = detail.context {
        print("Found context")
        contextField?.text = context.name
      }
    }
  }

  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == self.textField {
      detailItem?.text = textField.text
      do {
        try detailItem?.managedObjectContext?.save()
      } catch {
        print("Error saving MOC")
      }
    } else {
      let contextName = self.contextField.text!
      let fetchRequest = NSFetchRequest(entityName: "Context")
      fetchRequest.predicate = NSPredicate(format: "name == \"\(contextName)\"")
      let results: [Context]
      do {
        try results = self.detailItem?.managedObjectContext?.executeFetchRequest(fetchRequest) as! [Context]
        if results.count > 0 {
          detailItem?.context = results[0]
        } else {
          print("Creating new context")
          let moc = detailItem?.managedObjectContext
          let entity = NSEntityDescription.entityForName("Context", inManagedObjectContext: moc!)
          let context = Context(entity: entity!, insertIntoManagedObjectContext: moc)
          context.name = contextName
          detailItem?.context = context
          do {
            try moc?.save()
          } catch {
            abort()
          }
        }
      } catch {
        print("Fetch failed")
      }
    }
    return true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.configureView()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

