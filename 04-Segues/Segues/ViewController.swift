//
//  ViewController.swift
//  Segues
//
//  Created by Michael Lee on 6/6/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var label: UILabel!
  
  var val: Int?
  
  required init?(coder aDecoder: NSCoder) {
    self.val = 0
    super.init(coder: aDecoder)
  }
  
  override func viewWillAppear(animated: Bool) {
    label.text = "\(val)"
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "show" {
      let dest = segue.destinationViewController as! ViewController
      dest.val = self.val! + 1
    }
  }
  
  @IBAction func unwindHere(segue: UIStoryboardSegue) {
    
  }
}

