//
//  ViewController.swift
//  SQLiteDemo
//
//  Created by Michael Lee on 6/13/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet weak var tableView: UITableView!
  var todoItems = [(Int, String)]()
  var dbPath: String!
  var db: COpaquePointer!
  
  func openDatabase() -> COpaquePointer {
    var db: COpaquePointer = nil
    if sqlite3_open(dbPath, &db) == SQLITE_OK {
      print("Database opened")
      return db
    } else {
      return nil
    }
  }
  
  func initDatabase() {
    let createString = "CREATE TABLE todo (" +
    "id INTEGER PRIMARY KEY," +
    "text TEXT);"
    var createTableStatement: COpaquePointer = nil
    if sqlite3_prepare_v2(db, createString, -1, &createTableStatement, nil) == SQLITE_OK {
      if sqlite3_step(createTableStatement) == SQLITE_DONE {
        print("Table created")
      }
    }
    sqlite3_finalize(createTableStatement)
  }
  
  let insertString = "INSERT INTO todo (text) VALUES (?);"
  
  func insert(text: String) {
    var insertStatement: COpaquePointer = nil
    
    if sqlite3_prepare_v2(db, insertString, -1, &insertStatement, nil) == SQLITE_OK {
      sqlite3_bind_text(insertStatement, 1, (text as NSString).UTF8String, -1, nil)
      
      if sqlite3_step(insertStatement) == SQLITE_DONE {
        print("Inserted new todo")
      }
    }
    sqlite3_finalize(insertStatement)
  }
  
  let queryStatementString = "SELECT * FROM todo;"
  
  func query() {
    var queryStatement: COpaquePointer = nil
    if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
      todoItems = []
      while sqlite3_step(queryStatement) == SQLITE_ROW {
        let id = sqlite3_column_int(queryStatement, 0)
        let queryResultCol1 = sqlite3_column_text(queryStatement, 1)
        let text = String.fromCString(UnsafePointer<CChar>(queryResultCol1))!
        todoItems.append((Int(id), text))
        print("\(id) | \(text)")
      }
    }
    sqlite3_finalize(queryStatement)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let dirs = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .AllDomainsMask, true) as [NSString]
    dbPath = dirs[0].stringByAppendingPathComponent("db.sqlite3")
    print(dbPath)
    db = openDatabase()
//    initDatabase()
    query()
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return todoItems.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = todoItems[indexPath.row].1
    return cell
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    let text = textField.text!
    
    insert(text)
//    query()
//    self.tableView.reloadData()
    
    todoItems.append((-1, text))
    self.tableView.insertRowsAtIndexPaths(
      [NSIndexPath(forRow: todoItems.count-1, inSection: 0)],
      withRowAnimation: .Automatic)
    
    self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: todoItems.count-1, inSection: 0), atScrollPosition: .Bottom, animated: true)
    return true
  }

  override func finalize() {
    sqlite3_close(db)
  }
}

