//: Playground - noun: a place where people can play

import UIKit

var lst = [Int]()

var dict = [Int:String]()

func foo(a: Int) -> Int {
  return a + 1
}

func bar(widget a: Int, gadget b: Int) {
  print(a, b)
}

bar(widget: 10, gadget: 20)

func my_map<T>(lst: [T], fn: (T)->T) -> [T] {
  var ret = [T]()
  for x in lst {
    ret.append(fn(x))
  }
  return ret
}

my_map([1, 2, 3, 4], fn:foo)

let c = my_map([1, 2, 3, 4],
       fn:{
        (a: Int) -> Int in
        return a + 1
})
c

let d = my_map([1, 2, 3, 4]) { $0 + 1 }
d

let e = my_map(["hello", "there"], fn: { $0 })
e

class Node<T> {
  var val: T
  var next: Node<T>?
  
  init(val: T, next: Node<T>?) {
    self.val = val
    self.next = next
  }
}

class LinkedList<T> {
  var head: Node<T>? = nil
  
  func push(val: T) {
    head = Node<T>(val:val, next: head)
  }
  
  func pop() -> T? {
    if let n = head {
      let val = n.val
      head = n.next
      return val
    }
    return nil
  }
  
  /*
  // replace with property, below!
  func empty() -> Bool {
    return head == nil
  }
  */
  
  var empty: Bool {
    get {
      return head == nil
    }
  }
}

var l = LinkedList<Int>()

for i in 1...10 {
  l.push(i)
}

while !l.empty {
  print(l.pop()!)
}
