//
//  ViewController.swift
//  MapDemo
//
//  Created by Michael Lee on 6/22/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var tableView: UITableView!
  
  var locations = [(name: String, loc: CLLocation)]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loadJSONData()
  }
  
  func loadJSONData() {
    let path = NSBundle.mainBundle().pathForResource("landmarks", ofType: "json")!

    do {
      if let data = NSData(contentsOfFile: path) {
        let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
        let locs = json["data"] as! NSArray
        print("Name=\(locs[0][8]), loc=(\(locs[0][14]), \(locs[0][15])")
        locations = [(name: String, loc: CLLocation)]()
        for loc in locs {
          locations.append((name: loc[8] as! String, loc: CLLocation(latitude: Double(loc[14] as! String)!, longitude: Double(loc[15] as! String)!)))
        }
        tableView.reloadData()
      }
    } catch {
      abort()
    }
  }
  
  let regionRadius: CLLocationDistance = 1000
  func centerMapOnLocation(location: CLLocation) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                              regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
  }

  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return locations.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = locations[indexPath.row].name
    return cell
  }
  
  var mapAnnotation: MKPointAnnotation?
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let entry = locations[indexPath.row]
    centerMapOnLocation(entry.loc)
    
    if let annotation = mapAnnotation {
      annotation.coordinate = entry.loc.coordinate
      annotation.title = entry.name
    } else {
      mapAnnotation = MKPointAnnotation()
      mapAnnotation!.coordinate = entry.loc.coordinate
      mapAnnotation!.title = entry.name
      mapView.addAnnotation(mapAnnotation!)
    }
  }
}

