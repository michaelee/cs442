//
//  DetailViewController.swift
//  Todo
//
//  Created by Michael Lee on 6/6/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

  @IBOutlet weak var textField: UITextField!

  var detailItem: TodoItem? {
    didSet {
        // Update the view.
        self.configureView()
    }
  }

  func configureView() {
    // Update the user interface for the detail item.
    if let detail = self.detailItem {
        if let field = self.textField {
            field.text = detail.text
        }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.configureView()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  override func viewWillDisappear(animated: Bool) {
    detailItem?.text = textField.text
  }

}

