//
//  StackCalc.swift
//  RPNCalc
//
//  Created by Michael Lee on 6/8/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import Foundation

class StackCalc {
  var stack = [Int]()
  
  func push(val: Int) {
    stack.append(val)
  }
  
  func add() {
    if let x = stack.popLast() {
      if let y = stack.popLast() {
        stack.append(x + y)
      }
    }
  }
  
  func mult() {
    if let x = stack.popLast() {
      if let y = stack.popLast() {
        stack.append(x * y)
      }
    }
  }
  
  func values() -> [Int] {
    return stack.reverse()
  }
}