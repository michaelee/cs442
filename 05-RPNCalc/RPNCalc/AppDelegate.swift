//
//  AppDelegate.swift
//  RPNCalc
//
//  Created by Michael Lee on 6/8/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    let path = NSBundle.mainBundle().pathForResource("initsettings", ofType: "plist")
    let dict = NSDictionary(contentsOfFile: path!) as! [String:AnyObject]
    print(dict["name"])
    print(dict)

    return true
  }

}

