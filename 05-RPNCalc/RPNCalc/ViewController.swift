//
//  ViewController.swift
//  RPNCalc
//
//  Created by Michael Lee on 6/8/16.
//  Copyright © 2016 Michael Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource {
  @IBOutlet weak var tableView: UITableView!
  var calc = StackCalc()
  var stackVals: [Int]?
  
  func redisplay() {
    stackVals = calc.values()
    tableView.reloadData()
  }
  
  @IBAction func tapped(sender: AnyObject) {
    
  }
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    //    textField.resignFirstResponder()
    if let val = textField.text {
      calc.push(Int(val)!)
    }
    textField.text = ""
    redisplay()
    return true
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    switch string {
    case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
      return true
    case "+":
      calc.add()
    case "*":
      calc.mult()
    default:
      return false
    }
    redisplay()
    return false
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let vals = stackVals {
      return vals.count
    } else {
      return 0
    }
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "Stack"
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("stackCell")!
//    let cell = UITableViewCell()
    if let vals = stackVals {
      cell.textLabel?.text = "\(vals[indexPath.row])"
    }
    return cell
  }
}

